--------------------------------------------------------
--  File created - Monday-November-11-2019   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CAR
--------------------------------------------------------

  CREATE TABLE "CAR" 
   (	
    "ID" NUMBER(19,0) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 2 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"FABRICATION_DATE" DATE, 
	"VIN_NUMBER" VARCHAR2(255 CHAR), 
	"CAR_MODEL_ID" NUMBER(19,0), 
	"PURCHASE_ORDER_ID" NUMBER(19,0)
   );
--------------------------------------------------------
--  DDL for Table CAR_MODEL
--------------------------------------------------------

  CREATE TABLE "CAR_MODEL" 
   (	
    "ID" NUMBER(19,0) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 2 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"BASE_PRICE" FLOAT(126), 
	"IMAGE" BLOB, 
	"NAME" VARCHAR2(255 CHAR), 
	"TYPE" VARCHAR2(255 CHAR)
   );
--------------------------------------------------------
--  DDL for Table CAR_MODEL_FACTORY
--------------------------------------------------------

  CREATE TABLE "CAR_MODEL_FACTORY" 
   (	"CAR_MODEL_ID" NUMBER(19,0), 
	"FACTORY_ID" NUMBER(19,0)
   );
--------------------------------------------------------
--  DDL for Table CAR_MODEL_PARTS
--------------------------------------------------------

  CREATE TABLE "CAR_MODEL_PARTS" 
   (	"CAR_MODEL_ID" NUMBER(19,0), 
	"PARTS_ID" NUMBER(19,0)
   );
--------------------------------------------------------
--  DDL for Table CLIENT
--------------------------------------------------------

  CREATE TABLE "CLIENT" 
   (	
    "ID" NUMBER(19,0) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"NAME" VARCHAR2(255 CHAR),
    "BIRTHDATE" DATE,
    "CNP" NUMBER(13,0)
   );
--------------------------------------------------------
--  DDL for Table FACTORY
--------------------------------------------------------

CREATE TABLE REGION (
    id    NUMBER(19) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 2 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE ,
    name  VARCHAR2(255 CHAR)
);

CREATE TABLE COUNTRY (
    id         NUMBER(19) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 2 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE ,
    name       VARCHAR2(255 CHAR),
    region_id  NUMBER(19)
);

  CREATE TABLE "FACTORY" 
   (	
    "ID" NUMBER(19,0) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 2 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"NAME" VARCHAR2(255 CHAR),
    "COUNTRY_ID" NUMBER(19,0)
   );
--------------------------------------------------------
--  DDL for Table PART
--------------------------------------------------------

  CREATE TABLE "PART" 
   (	
    "ID" NUMBER(19,0) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 2 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"DESCRIPTION" VARCHAR2(255 CHAR), 
	"NAME" VARCHAR2(255 CHAR), 
	"QUANTITY" NUMBER(10,0), 
	"FACTORY_ID" NUMBER(19,0)
   );
--------------------------------------------------------
--  DDL for Table PART_SUPPLIER
--------------------------------------------------------

  CREATE TABLE "PART_SUPPLIER" 
   (	"PART_ID" NUMBER(19,0), 
	"SUPPLIER_ID" NUMBER(19,0)
   );
--------------------------------------------------------
--  DDL for Table PURCHASE_ORDER
--------------------------------------------------------

  CREATE TABLE "PURCHASE_ORDER" 
   (	
    "ID" NUMBER(19,0) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 2 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"CODE" VARCHAR2(255 CHAR), 
	"COMPLETED" NUMBER(1,0), 
	"COST" FLOAT(126), 
	"REQUEST_DATE" DATE, 
	"CLIENT_ID" NUMBER(19,0), 
	"FACTORY_ID" NUMBER(19,0)
   );
--------------------------------------------------------
--  DDL for Table SUPPLIER
--------------------------------------------------------

  CREATE TABLE "SUPPLIER" 
   (	
    "ID" NUMBER(19,0) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE , 
	"NAME" VARCHAR2(255 CHAR),
    "ADDRESS" VARCHAR2(255 CHAR)
   );
--------------------------------------------------------
--  DDL for Index SYS_C007427
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C017427" ON "CAR" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C007429
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C017429" ON "CAR_MODEL" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C007435
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C017435" ON "CAR_MODEL_FACTORY" ("FACTORY_ID", "CAR_MODEL_ID");
--------------------------------------------------------
--  DDL for Index UK_5J8QHDM1CV3VOVL8IG0M8S16T
--------------------------------------------------------

  CREATE UNIQUE INDEX "UK_1J8QHDM1CV3VOVL8IG0M8S16T" ON "CAR_MODEL_PARTS" ("PARTS_ID");
--------------------------------------------------------
--  DDL for Index SYS_C007432
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C017432" ON "CAR_MODEL_PARTS" ("CAR_MODEL_ID", "PARTS_ID");
--------------------------------------------------------
--  DDL for Index SYS_C007437
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C017437" ON "CLIENT" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C007439
--------------------------------------------------------
  CREATE UNIQUE INDEX "SYS_C017433" ON "COUNTRY" ("ID");
  
  CREATE UNIQUE INDEX "SYS_C017467" ON "REGION" ("ID");

  CREATE UNIQUE INDEX "SYS_C017439" ON "FACTORY" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C007442
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C017442" ON "PART" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C007445
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C017445" ON "PART_SUPPLIER" ("SUPPLIER_ID", "PART_ID");
--------------------------------------------------------
--  DDL for Index SYS_C007449
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C017449" ON "PURCHASE_ORDER" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C007451
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C017451" ON "SUPPLIER" ("ID");
--------------------------------------------------------
--  Constraints for Table CAR
--------------------------------------------------------

  ALTER TABLE "CAR" MODIFY ("FABRICATION_DATE" NOT NULL ENABLE);
  ALTER TABLE "CAR" MODIFY ("VIN_NUMBER" NOT NULL ENABLE);
  ALTER TABLE "CAR" ADD CONSTRAINT "UK_4DGQHDM1CV3VOVLASE0M8S16T" UNIQUE ("VIN_NUMBER");
  ALTER TABLE "CAR" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table CAR_MODEL
--------------------------------------------------------

  ALTER TABLE "CAR_MODEL" MODIFY ("BASE_PRICE" NOT NULL ENABLE);
  ALTER TABLE "CAR_MODEL" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "CAR_MODEL" MODIFY ("TYPE" NOT NULL ENABLE);
  ALTER TABLE "CAR_MODEL" ADD CONSTRAINT "UK_ZACQHDM1CV3VOVLABC0M8S16T" UNIQUE ("NAME");
  ALTER TABLE "CAR_MODEL" ADD CONSTRAINT check_positive_cost_car CHECK (BASE_PRICE > 0);
  ALTER TABLE "CAR_MODEL" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table CAR_MODEL_FACTORY
--------------------------------------------------------

  ALTER TABLE "CAR_MODEL_FACTORY" MODIFY ("CAR_MODEL_ID" NOT NULL ENABLE);
  ALTER TABLE "CAR_MODEL_FACTORY" MODIFY ("FACTORY_ID" NOT NULL ENABLE);
  ALTER TABLE "CAR_MODEL_FACTORY" ADD PRIMARY KEY ("FACTORY_ID", "CAR_MODEL_ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table CAR_MODEL_PARTS
--------------------------------------------------------

  ALTER TABLE "CAR_MODEL_PARTS" MODIFY ("CAR_MODEL_ID" NOT NULL ENABLE);
  ALTER TABLE "CAR_MODEL_PARTS" MODIFY ("PARTS_ID" NOT NULL ENABLE);
  ALTER TABLE "CAR_MODEL_PARTS" ADD CONSTRAINT "UK_1J8QHDM1CV3VOVL8IG0M8S16T" UNIQUE ("PARTS_ID");
  ALTER TABLE "CAR_MODEL_PARTS" ADD PRIMARY KEY ("CAR_MODEL_ID", "PARTS_ID")
  USING INDEX  ENABLE;
  
--------------------------------------------------------
--  Constraints for Table CLIENT
--------------------------------------------------------

  ALTER TABLE "CLIENT" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "CLIENT" MODIFY ("BIRTHDATE" NOT NULL ENABLE);
  ALTER TABLE "CLIENT" MODIFY ("CNP" NOT NULL ENABLE);
  ALTER TABLE "CLIENT" ADD CONSTRAINT "UK_IUKIFDM1CV3VOVL8ABAB8S16T" UNIQUE ("CNP");
  ALTER TABLE "CLIENT" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table FACTORY
--------------------------------------------------------
  
  ALTER TABLE "REGION" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "REGION" ADD CONSTRAINT "UK_YRTIKDM1CV3VOVL8ABAB8SDFR" UNIQUE ("NAME");
  ALTER TABLE "REGION" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;  
  
  ALTER TABLE "COUNTRY" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "COUNTRY" ADD CONSTRAINT "UK_HHKIFDM1CV3VOVL8ABAB8SABO" UNIQUE ("NAME");
  ALTER TABLE "COUNTRY" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;

  ALTER TABLE "FACTORY" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "FACTORY" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table PART
--------------------------------------------------------

  ALTER TABLE "PART" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "PART" ADD CONSTRAINT "UK_UILNMDM1CV3VOVL8ABAB8S55D" UNIQUE ("NAME");
  ALTER TABLE "PART" MODIFY ("QUANTITY" NOT NULL ENABLE);
  ALTER TABLE "PART" ADD CONSTRAINT check_positive_cost_part CHECK (QUANTITY > 0);
  ALTER TABLE "PART" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table PART_SUPPLIER
--------------------------------------------------------

  ALTER TABLE "PART_SUPPLIER" MODIFY ("PART_ID" NOT NULL ENABLE);
  ALTER TABLE "PART_SUPPLIER" MODIFY ("SUPPLIER_ID" NOT NULL ENABLE);
  ALTER TABLE "PART_SUPPLIER" ADD PRIMARY KEY ("SUPPLIER_ID", "PART_ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table PURCHASE_ORDER
--------------------------------------------------------

  ALTER TABLE "PURCHASE_ORDER" MODIFY ("COMPLETED" NOT NULL ENABLE);
  ALTER TABLE "PURCHASE_ORDER" MODIFY ("COST" NOT NULL ENABLE);
  ALTER TABLE "PURCHASE_ORDER" MODIFY ("REQUEST_DATE" NOT NULL ENABLE);
  ALTER TABLE "PURCHASE_ORDER" ADD CONSTRAINT check_positive_cost_purchase CHECK (COST > 0);
  ALTER TABLE "PURCHASE_ORDER" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table SUPPLIER
--------------------------------------------------------

  ALTER TABLE "SUPPLIER" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "SUPPLIER" MODIFY ("ADDRESS" NOT NULL ENABLE);
  ALTER TABLE "SUPPLIER" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CAR
--------------------------------------------------------

  ALTER TABLE "FACTORY" ADD CONSTRAINT "FK1FHT1L5LCN8XBUGHA10MNT4BG" FOREIGN KEY ("COUNTRY_ID")
	  REFERENCES "COUNTRY" ("ID") ENABLE;
      
  ALTER TABLE "COUNTRY" ADD CONSTRAINT "FK1FHT1T5LCN8GBUGHA10MNT4BG" FOREIGN KEY ("REGION_ID")
      REFERENCES "REGION" ("ID") ENABLE;

  ALTER TABLE "CAR" ADD CONSTRAINT "FK1FHT1L5LCN8GBUGHA10MNT4BG" FOREIGN KEY ("CAR_MODEL_ID")
	  REFERENCES "CAR_MODEL" ("ID") ENABLE;
  ALTER TABLE "CAR" ADD CONSTRAINT "FK1676QLA2MPY21HLFKEYAF62YD" FOREIGN KEY ("PURCHASE_ORDER_ID")
	  REFERENCES "PURCHASE_ORDER" ("ID") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CAR_MODEL_FACTORY
--------------------------------------------------------

  ALTER TABLE "CAR_MODEL_FACTORY" ADD CONSTRAINT "FK16M4KBMJQRKGU0YFAUTS80CPB" FOREIGN KEY ("FACTORY_ID")
	  REFERENCES "FACTORY" ("ID") ENABLE;
  ALTER TABLE "CAR_MODEL_FACTORY" ADD CONSTRAINT "FK1KWIKXWRCROB2NHHFS2989K5" FOREIGN KEY ("CAR_MODEL_ID")
	  REFERENCES "CAR_MODEL" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CAR_MODEL_PARTS
--------------------------------------------------------

  ALTER TABLE "CAR_MODEL_PARTS" ADD CONSTRAINT "FK13RKGS9YVX4SAXI5H6U0QJ16X" FOREIGN KEY ("PARTS_ID")
	  REFERENCES "PART" ("ID") ENABLE;
  ALTER TABLE "CAR_MODEL_PARTS" ADD CONSTRAINT "FK1PG4NVAOP23E627KEOQR0IVOT" FOREIGN KEY ("CAR_MODEL_ID")
	  REFERENCES "CAR_MODEL" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PART
--------------------------------------------------------

  ALTER TABLE "PART" ADD CONSTRAINT "FK1H3YY5WHVSV33OM40J9Q5O25Q" FOREIGN KEY ("FACTORY_ID")
	  REFERENCES "FACTORY" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PART_SUPPLIER
--------------------------------------------------------

  ALTER TABLE "PART_SUPPLIER" ADD CONSTRAINT "FK1T8HE44H937JCQ3G4BIQ7Q8YA" FOREIGN KEY ("SUPPLIER_ID")
	  REFERENCES "SUPPLIER" ("ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "PART_SUPPLIER" ADD CONSTRAINT "FK17QAMN70B5QFYT8VY7HOP8E4F" FOREIGN KEY ("PART_ID")
	  REFERENCES "PART" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PURCHASE_ORDER
--------------------------------------------------------

  ALTER TABLE "PURCHASE_ORDER" ADD CONSTRAINT "FK10V1B5YWMK0N0RASY577MCIJX" FOREIGN KEY ("CLIENT_ID")
	  REFERENCES "CLIENT" ("ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "PURCHASE_ORDER" ADD CONSTRAINT "FK1L17MATRNP47BK0JIXXB4YUC5" FOREIGN KEY ("FACTORY_ID")
	  REFERENCES "FACTORY" ("ID") ENABLE;
