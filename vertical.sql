CREATE OR REPLACE PROCEDURE INSERT_PART_SUPPLIER(supplierId number, partId number)
IS
BEGIN
    INSERT INTO part_supplier (part_id, supplier_id) VALUES (partId, supplierId);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_SUPPLIER_AS(name varchar, address varchar, email varchar, supplierId out number)
IS
BEGIN
    INSERT INTO SUPPLIER (email) VALUES (email);
    INSERT INTO SUPPLIER@dblink_eu (name, address) VALUES (name, address);
    SELECT MAX(id) INTO supplierId FROM SUPPLIER; 
END;
/

CREATE OR REPLACE PROCEDURE INSERT_SUPPLIER_EU(name varchar, address varchar, email varchar, supplierId out number)
IS
BEGIN
    INSERT INTO SUPPLIER@dblink_as (email) VALUES (email);
    INSERT INTO SUPPLIER (name, address) VALUES (name, address);
    SELECT MAX(id) INTO supplierId FROM SUPPLIER; 
END;
/

CREATE OR REPLACE PROCEDURE UPDATE_SUPPLIER_AS(new_id number, new_name varchar, new_address varchar, new_email varchar)
IS
BEGIN
    UPDATE SUPPLIER SET email = new_email where id = new_id;
    UPDATE SUPPLIER@dblink_eu SET name = new_name, address = new_address where id = new_id;
END;
/

CREATE OR REPLACE PROCEDURE UPDATE_SUPPLIER_EU(new_id number, new_name varchar, new_address varchar, new_email varchar)
IS
BEGIN
    UPDATE SUPPLIER@dblink_as SET email = new_email where id = new_id;
    UPDATE SUPPLIER SET name = new_name, address = new_address where id = new_id;
END;
/

CREATE OR REPLACE PROCEDURE delete_supplier(old_id number)
IS
BEGIN
DELETE FROM SUPPLIER WHERE id = old_id;
DELETE FROM SUPPLIER@dblink_eu WHERE id = old_id;
END;
/

CREATE OR REPLACE PROCEDURE delete_supplier(old_id number)
IS
BEGIN
DELETE FROM SUPPLIER WHERE id = old_id;
DELETE FROM SUPPLIER@dblink_as WHERE id = old_id;
END;
/

CREATE OR REPLACE PROCEDURE DELETE_PART_SUPPLIERS(supplierId number)
IS
BEGIN
    DELETE FROM part_supplier where supplier_id = supplierId;
END;
/



