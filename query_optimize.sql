--Afisarea procentului de ordine de plata, la nivel global care au statusul completated, din luna precedenta, costul total, numele tarii, 
--numele regiunii si numele fabricii, groupate dupa fabrica, ordonate in ordine descrescatoare dupa procent.
exec dbms_stats.gather_schema_stats (ownname => 'app_global');

-- Cost

explain plan
set statement_id = 'total_cost'
FOR
SELECT sum(po.cost) + (SELECT sum(cm.base_price) from car c join car_model cm on c.car_model_id = cm.id where c.purchase_order_id in (SELECT ppo.id from purchase_order ppo where ppo.factory_id = f.id)) "Total cost", count(po.completed) "Completed orders",
ROUND(count(po.completed) / (select count(pp.id) from purchase_order pp where pp.factory_id = f.id) * 100, 0) as "Completed %",
r.name "Region", c.name "Country", f.name "Factory" 
FROM PURCHASE_ORDER po 
JOIN FACTORY f ON po.factory_id = f.id 
JOIN COUNTRY c ON f.country_id = c.id 
JOIN REGION r ON c.region_id = r.id
WHERE po.COMPLETED = 1 AND po.REQUEST_DATE BETWEEN trunc (ADD_MONTHS(SYSDATE, -12), 'mm') AND ADD_MONTHS(SYSDATE, -1)
GROUP BY f.id, f.name, c.name, r.name
ORDER BY "Completed %" desc;


SELECT plan_table_output FROM 
table(dbms_xplan.display('plan_table', 'total_cost', 'serial'));

-- Rule

explain plan
set statement_id = 'total_cost'
FOR
SELECT /*+RULE*/ sum(po.cost) + (SELECT sum(cm.base_price) from car c join car_model cm on c.car_model_id = cm.id where c.purchase_order_id in (SELECT ppo.id from purchase_order ppo where ppo.factory_id = f.id)) "Total cost", count(po.completed) "Completed orders",
ROUND(count(po.completed) / (select count(pp.id) from purchase_order pp where pp.factory_id = f.id) * 100, 0) as "Completed %",
r.name "Region", c.name "Country", f.name "Factory" 
FROM PURCHASE_ORDER po 
JOIN FACTORY f ON po.factory_id = f.id 
JOIN COUNTRY c ON f.country_id = c.id 
JOIN REGION r ON c.region_id = r.id
WHERE po.COMPLETED = 1 AND po.REQUEST_DATE BETWEEN trunc (ADD_MONTHS(SYSDATE, -12), 'mm') AND ADD_MONTHS(SYSDATE, -1)
GROUP BY f.id, f.name, c.name, r.name
ORDER BY "Completed %" desc;

SELECT plan_table_output FROM 
table(dbms_xplan.display('plan_table', 'total_cost', 'serial'));

--INDEX JOIN on Country

explain plan
set statement_id = 'total_cost'
FOR
SELECT /*+INDEX(c SYS_C007433) FULL(r) INDEX_JOIN(f SYS_C007439 SYS_C007433) INDEX_JOIN(po SYS_C007449 SYS_C007439)*/ 
sum(po.cost) + (SELECT /*+INDEX(c SYS_C007427)*/ NVL(sum(cm.base_price),0) 
from car c join car_model cm on c.car_model_id = cm.id 
where c.purchase_order_id in (SELECT ppo.id from purchase_order ppo where ppo.factory_id = f.id)) "Total cost", count(po.completed) "Completed orders",
ROUND(count(po.completed) / (select count(pp.id) from purchase_order pp where pp.factory_id = f.id) * 100, 0) as "Completed %",
r.name "Region", c.name "Country", f.name "Factory" 
FROM PURCHASE_ORDER po 
JOIN FACTORY f ON po.factory_id = f.id 
JOIN COUNTRY c ON f.country_id = c.id 
JOIN REGION r ON c.region_id = r.id
WHERE po.COMPLETED = 1 AND po.REQUEST_DATE BETWEEN trunc (ADD_MONTHS(SYSDATE, -12), 'mm') AND ADD_MONTHS(SYSDATE, -1)
GROUP BY f.id, f.name, c.name, r.name
ORDER BY "Completed %" desc;


SELECT plan_table_output FROM 
table(dbms_xplan.display('plan_table', 'total_cost', 'serial'));
