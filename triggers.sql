--insert

CREATE OR REPLACE TRIGGER trig_replicate_client_as
BEFORE INSERT ON CLIENT_AS
FOR EACH ROW
BEGIN
ALTER TRIGGER trig_replicate_client_eu@dblink_eu DISABLE;
INSERT INTO CLIENT_EU@dblink_eu (name, birthdate, cnp) 
VALUES (:NEW.name, :NEW.birthdate, :NEW.cnp);
ALTER TRIGGER trig_replicate_client_eu@dblink_eu DISABLE;
END;
/

CREATE OR REPLACE TRIGGER trig_replicate_client_eu
BEFORE INSERT ON CLIENT_EU
FOR EACH ROW
BEGIN
INSERT INTO CLIENT_AS@dblink_as (name, birthdate, cnp) 
VALUES (:NEW.name, :NEW.birthdate, :NEW.cnp);
END;
/

--update

CREATE OR REPLACE TRIGGER trig_update_client_as
BEFORE UPDATE ON CLIENT_AS
FOR EACH ROW
BEGIN
UPDATE CLIENT_EU@dblink_eu SET name = :NEW.name, birthdate = :NEW.birthdate, cnp = :NEW.cnp
WHERE id = :NEW.id;
END;
/

CREATE OR REPLACE TRIGGER trig_update_client_eu
BEFORE UPDATE ON CLIENT_EU
FOR EACH ROW
BEGIN
UPDATE CLIENT_AS@dblink_as SET name = :NEW.name, birthdate = :NEW.birthdate, cnp = :NEW.cnp
WHERE id = :NEW.id;
END;
/

--delete

CREATE OR REPLACE TRIGGER trig_update_client_as
BEFORE DELETE ON CLIENT_AS
FOR EACH ROW
BEGIN
DELETE FROM CLIENT_EU@dblink_eu
WHERE id = :OLD.id;
END;
/

CREATE OR REPLACE TRIGGER trig_update_client_eu
BEFORE DELETE ON CLIENT_EU
FOR EACH ROW
BEGIN
DELETE FROM CLIENT_AS@dblink_as
WHERE id = :OLD.id;
END;
/