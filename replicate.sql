--Europe

CREATE OR REPLACE PROCEDURE add_client(name varchar, birthdate date, cnp number)
IS
BEGIN
INSERT INTO CLIENT (name, birthdate, cnp) values (name, birthdate, cnp);
INSERT INTO CLIENT@dblink_as (name, birthdate, cnp) values (name, birthdate, cnp);
END;
/

CREATE OR REPLACE PROCEDURE update_client(new_id number, new_name varchar, new_birthdate date, new_cnp number)
IS
BEGIN
UPDATE CLIENT set name = new_name, birthdate = new_birthdate, cnp = new_cnp where id = new_id;
UPDATE CLIENT@dblink_as set name = new_name, birthdate = new_birthdate, cnp = new_cnp where id = new_id;
END;
/

CREATE OR REPLACE PROCEDURE delete_client(old_id number)
IS
BEGIN
DELETE FROM CLIENT WHERE id = old_id;
DELETE FROM CLIENT@dblink_as WHERE id = old_id;
END;
/

--Asia

CREATE OR REPLACE PROCEDURE add_client(name varchar, birthdate date, cnp number)
IS
BEGIN
INSERT INTO CLIENT (name, birthdate, cnp) values (name, birthdate, cnp);
INSERT INTO CLIENT@dblink_eu (name, birthdate, cnp) values (name, birthdate, cnp);
END;
/

CREATE OR REPLACE PROCEDURE update_client(new_id number, new_name varchar, new_birthdate date, new_cnp number)
IS
BEGIN
UPDATE CLIENT set name = new_name, birthdate = new_birthdate, cnp = new_cnp where id = new_id;
UPDATE CLIENT@dblink_eu set name = new_name, birthdate = new_birthdate, cnp = new_cnp where id = new_id;
END;
/

CREATE OR REPLACE PROCEDURE delete_client(old_id number)
IS
BEGIN
DELETE FROM CLIENT WHERE id = old_id;
DELETE FROM CLIENT@dblink_eu WHERE id = old_id;
END;
/