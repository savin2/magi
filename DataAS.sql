--Replicare

INSERT INTO CLIENT (name, birthdate, cnp)
SELECT name, birthdate, cnp FROM CLIENT@dblink_global 
ORDER BY id;


--Fragmentare orizontala primara

INSERT INTO REGION (name)
SELECT name FROM REGION@dblink_global where name = 'Asia';

INSERT INTO COUNTRY (name, region_id)
SELECT c.name, c.region_id FROM COUNTRY@dblink_global c JOIN REGION r on c.region_id = r.id;

SELECT * FROM country_eu@dblink_eu
UNION ALL
SELECT * FROM country_as;

-- Fragmentare orizontala derivata

INSERT INTO FACTORY (name, country_id)
SELECT f.name, f.country_id FROM FACTORY@dblink_global f JOIN COUNTRY c on f.country_id = c.id;

INSERT INTO PURCHASE_ORDER (client_id, code, completed, cost, request_date, factory_id)
SELECT p.client_id, p.code, p.completed, p.cost, p.request_date, p.factory_id from PURCHASE_ORDER@dblink_global p JOIN FACTORY f on p.factory_id = f.id;

INSERT INTO CAR_MODEL (name, base_price, type)
SELECT DISTINCT c.name, c.base_price, c.type FROM CAR_MODEL@dblink_global c JOIN CAR_MODEL_FACTORY@dblink_global cmf ON cmf.car_model_id = c.id JOIN FACTORY f on f.id = cmf.factory_id;

INSERT INTO CAR_MODEL_FACTORY
SELECT cmf.car_model_id, cmf.factory_id FROM CAR_MODEL_FACTORY@dblink_global cmf JOIN CAR_MODEL cm on cmf.car_model_id = cm.id JOIN FACTORY f on cmf.factory_id = f.id;

INSERT INTO CAR (car_model_id, fabrication_date, vin_number, purchase_order_id)
SELECT DISTINCT c.car_model_id, c.fabrication_date, c.vin_number, c.purchase_order_id FROM CAR@dblink_global c JOIN CAR_MODEL cm on c.car_model_id = cm.id JOIN PURCHASE_ORDER po ON c.purchase_order_id = po.id;

INSERT INTO PART (name, description, quantity, factory_id)
SELECT p.name, p.description, p.quantity, p.factory_id FROM PART@dblink_global p JOIN FACTORY f ON p.factory_id = f.id;

INSERT INTO CAR_MODEL_PARTS
SELECT cmp.car_model_id, cmp.parts_id FROM CAR_MODEL_PARTS@dblink_global cmp JOIN CAR_MODEL cm ON cmp.car_model_id = cm.id JOIN PART p ON cmp.parts_id = p.id;

-- Fragmentare verticala

INSERT INTO SUPPLIER (email)
SELECT email FROM SUPPLIER@dblink_global ORDER BY id;

INSERT INTO PART_SUPPLIER
SELECT ps.part_id, ps.supplier_id FROM PART_SUPPLIER@dblink_global ps JOIN PART p ON ps.part_id = p.id JOIN SUPPLIER s ON ps.supplier_id = s.id;

SELECT s.name, s.address, s_as.email
FROM SUPPLIER s_as JOIN SUPPLIER@dblink_eu s ON s_as.id = s.id;