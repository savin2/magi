CREATE DATABASE LINK dblink_as
    CONNECT TO app_user IDENTIFIED BY secret
    USING '(DESCRIPTION=
                (ADDRESS=(PROTOCOL=TCP)(HOST=magi2020.duckdns.org)(PORT=7722))
                (CONNECT_DATA=(SERVICE_NAME=ORCLCDB.localdomain))
            )';
            
CREATE DATABASE LINK dblink_eu 
    CONNECT TO app_user IDENTIFIED BY secret
    USING '(DESCRIPTION=
                (ADDRESS=(PROTOCOL=TCP)(HOST=magi2020.duckdns.org)(PORT=7721))
                (CONNECT_DATA=(SERVICE_NAME=ORCLCDB.localdomain))
            )';
			
CREATE DATABASE LINK dblink_global 
    CONNECT TO app_global IDENTIFIED BY secret
    USING '(DESCRIPTION=
                (ADDRESS=(PROTOCOL=TCP)(HOST=magi2020.duckdns.org)(PORT=7720))
                (CONNECT_DATA=(SERVICE_NAME=ORCLCDB.localdomain))
            )';