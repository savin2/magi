package com.automotive.AutomotiveFactory.model;

import lombok.*;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NamedStoredProcedureQuery(name = "delete_client", procedureName = "delete_client",
        parameters = {@StoredProcedureParameter(name = "old_id", type=Long.class, mode=ParameterMode.IN) })
@NamedStoredProcedureQuery(name = "update_client", procedureName = "update_client",
        parameters = {@StoredProcedureParameter(name = "new_id", type=Long.class, mode=ParameterMode.IN),
                      @StoredProcedureParameter(name = "new_name", type=String.class, mode=ParameterMode.IN),
                      @StoredProcedureParameter(name = "new_birthdate", type=Date.class, mode=ParameterMode.IN),
                      @StoredProcedureParameter(name = "new_cnp", type=Long.class, mode=ParameterMode.IN)})
@NamedStoredProcedureQuery(name = "add_client", procedureName = "add_client",
        parameters = {@StoredProcedureParameter(name = "name", type=String.class, mode=ParameterMode.IN),
                      @StoredProcedureParameter(name = "birthdate", type=Date.class, mode=ParameterMode.IN),
                      @StoredProcedureParameter(name = "cnp", type=Long.class, mode=ParameterMode.IN)})
@Table(name="CLIENT")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date birthdate;
    private Long CNP;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client")
    private Set<PurchaseOrder> purchaseOrders = new HashSet<>();


}
