package com.automotive.AutomotiveFactory.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name="SUPPLIER")
@NamedStoredProcedureQuery(name = "delete_supplier", procedureName = "delete_supplier",
        parameters = {@StoredProcedureParameter(name = "old_id", type=Long.class, mode=ParameterMode.IN) })
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address;
    private String email;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "PartSupplier",
            joinColumns = @JoinColumn(name = "supplier_id"),
            inverseJoinColumns = @JoinColumn(name = "part_id"))
    private Set<Part> parts = new HashSet<>();

}
