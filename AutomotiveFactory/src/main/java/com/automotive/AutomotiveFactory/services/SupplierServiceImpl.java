package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.SupplierCommand;
import com.automotive.AutomotiveFactory.converters.CommandToSupplierConverter;
import com.automotive.AutomotiveFactory.converters.SupplierToCommandConverter;
import com.automotive.AutomotiveFactory.model.Part;
import com.automotive.AutomotiveFactory.model.Supplier;
import com.automotive.AutomotiveFactory.repositories.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class SupplierServiceImpl implements SupplierService {

    SupplierRepository supplierRepository;
    PartRepository partRepository;
    RegionRepository regionRepository;
    SupplierToCommandConverter supplierToCommandConverter;
    CommandToSupplierConverter commandToSupplierConverter;
    EntityManager entityManager;

    public Set<Supplier> getSuppliers(){
        int regionID = regionRepository.getRegionId();

        Set<Supplier> suppliers = new HashSet<Supplier>();

        if( regionID == 1) //EUROPA
        {
            Set<SupplierFromEU> euSuppliers = supplierRepository.findAllEUSuppliers();

            for(SupplierFromEU sfeu : euSuppliers) {
                Supplier supplier = new Supplier();

                supplier.setId(sfeu.getId());
                supplier.setName(sfeu.getName());
                supplier.setAddress(sfeu.getAddress());
                supplier.setEmail("");
                supplier.setParts(sfeu.getParts());

                suppliers.add(supplier);
            }
        }
        else //ASIA
        {
            Set<SupplierFromAS> asSuppliers = supplierRepository.findAllASSuppliers();

            for(SupplierFromAS aseu : asSuppliers) {
                Supplier supplier = new Supplier();

                supplier.setId(aseu.getId());
                supplier.setName("");
                supplier.setAddress("");
                supplier.setEmail(aseu.getEmail());
                supplier.setParts(aseu.getParts());

                suppliers.add(supplier);
            }
        }

        return suppliers;
    }

    public Supplier getSupplier(Long id){
        int regionID = regionRepository.getRegionId();

        List<Long> partIds = supplierRepository.findPartIds(id);
        Set<Part> parts = partRepository.findPartsByIdIn(partIds);

        Supplier supplier = new Supplier();


        if(regionID == 1) //EUROPA
        {
            SupplierCustom seu = supplierRepository.findEUSupplierById(id);

            supplier.setId(seu.getId());
            supplier.setName(seu.getName());
            supplier.setAddress(seu.getAddress());
            supplier.setEmail(seu.getEmail());
            supplier.setParts(parts);
        }
        else
        {
            SupplierCustom sas = supplierRepository.findASSupplierById(id);

            supplier.setId(sas.getId());
            supplier.setName(sas.getName());
            supplier.setAddress(sas.getAddress());
            supplier.setEmail(sas.getEmail());
            supplier.setParts(parts);
        }

        return supplier;
    }

    public SupplierCommand getSupplierCommand(Long id){
        return supplierToCommandConverter.convert(getSupplier(id));
    }

    public Supplier save(Supplier supplier){
        return supplierRepository.save(supplier);
    }

    public void saveSupplierCommand(SupplierCommand supplierCommand){
        int regionID = regionRepository.getRegionId();

        if(supplierCommand.getId() != null) {
            entityManager.createStoredProcedureQuery("delete_part_suppliers")
                    .registerStoredProcedureParameter("supplierId", Long.class, ParameterMode.IN)
                    .setParameter("supplierId", supplierCommand.getId())
                    .execute();
            if(regionID == 1) //EUROPA
            {
                entityManager.createStoredProcedureQuery("update_supplier_eu")
                        .registerStoredProcedureParameter("new_id", Long.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("new_name", String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("new_address", String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("new_email", String.class, ParameterMode.IN)
                        .setParameter("new_id", supplierCommand.getId())
                        .setParameter("new_name", supplierCommand.getName())
                        .setParameter("new_address", supplierCommand.getAddress())
                        .setParameter("new_email", supplierCommand.getEmail())
                        .execute();
            }
            else //ASIA
            {
                entityManager.createStoredProcedureQuery("update_supplier_as")
                        .registerStoredProcedureParameter("new_id", Long.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("new_name", String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("new_address", String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("new_email", String.class, ParameterMode.IN)
                        .setParameter("new_id", supplierCommand.getId())
                        .setParameter("new_name", supplierCommand.getName())
                        .setParameter("new_address", supplierCommand.getAddress())
                        .setParameter("new_email", supplierCommand.getEmail())
                        .execute();
            }

        }
        else {

            if (regionID == 1) //EUROPA
            {
                entityManager.createStoredProcedureQuery("insert_supplier_eu")
                        .registerStoredProcedureParameter("id", Long.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("name", String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("address", String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("email", String.class, ParameterMode.IN)
                        .setParameter("id", supplierCommand.getId())
                        .setParameter("name", supplierCommand.getName())
                        .setParameter("address", supplierCommand.getAddress())
                        .setParameter("email", supplierCommand.getEmail()).execute();


            } else //ASIA
            {

                entityManager.createStoredProcedureQuery("insert_supplier_as")
                        .registerStoredProcedureParameter("id", Long.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("name", String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("address", String.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("email", String.class, ParameterMode.IN)
                        .setParameter("id", supplierCommand.getId())
                        .setParameter("name", supplierCommand.getName())
                        .setParameter("address", supplierCommand.getAddress())
                        .setParameter("email", supplierCommand.getEmail()).execute();
                }
            }

            for (Long id : supplierCommand.getPartIds()) {
                entityManager.createStoredProcedureQuery("insert_part_supplier")
                        .registerStoredProcedureParameter("supplierId", Long.class, ParameterMode.IN)
                        .registerStoredProcedureParameter("partId", Long.class, ParameterMode.IN)
                        .setParameter("supplierId", supplierCommand.getId())
                        .setParameter("partId", id)
                        .execute();
            }


    }

    public void delete(Long id){
        entityManager.createNamedStoredProcedureQuery("delete_supplier")
                .setParameter("old_id", id)
                .execute();
    }

}
