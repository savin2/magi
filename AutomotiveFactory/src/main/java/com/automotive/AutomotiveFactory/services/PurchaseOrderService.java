package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.PurchaseOrderCommand;
import com.automotive.AutomotiveFactory.model.PurchaseOrder;

import java.util.Set;

public interface PurchaseOrderService {

    Set<PurchaseOrder> getPurchaseOrders();
    PurchaseOrder getPurchaseOrder(Long id);
    PurchaseOrderCommand getPurchaseOrderCommand(Long id);
    void deletePurchaseOrder(Long id);
    PurchaseOrderCommand savePurchaseOrderCommand(PurchaseOrderCommand order);

}
