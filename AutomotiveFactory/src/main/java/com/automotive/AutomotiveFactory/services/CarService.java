package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.CarCommand;
import com.automotive.AutomotiveFactory.model.Car;

import java.util.Set;

public interface CarService {

    Set<Car> getCars();
    Car getCar(Long id);
    Car saveCar(Car car);
    CarCommand getCarCommand(Long id);
    CarCommand saveCarCommand(CarCommand carCommand);
    void deleteCar(Long id);

}
