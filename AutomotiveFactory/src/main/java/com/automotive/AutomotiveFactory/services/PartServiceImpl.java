package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.PartCommand;
import com.automotive.AutomotiveFactory.converters.CommandToPartConverter;
import com.automotive.AutomotiveFactory.converters.PartToCommandConverter;
import com.automotive.AutomotiveFactory.model.Part;
import com.automotive.AutomotiveFactory.model.Supplier;
import com.automotive.AutomotiveFactory.repositories.PartRepository;
import com.automotive.AutomotiveFactory.repositories.RegionRepository;
import com.automotive.AutomotiveFactory.repositories.SupplierRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class PartServiceImpl implements PartService {

    PartRepository partRepository;
    SupplierRepository supplierRepository;
    RegionRepository regionRepository;
    PartToCommandConverter partToCommandConverter;
    CommandToPartConverter commandToPartConverter;

    public Set<Part> getParts(){
        int regionID = regionRepository.getRegionId();
        Set<Part> parts = new HashSet<>();
        partRepository.findAll().iterator().forEachRemaining(parts::add);


        for(Part p : parts){
            Set<Supplier> suppliers = new HashSet<>();
            if(regionID == 1) // Europe
            {
                List<String> supplierNames = partRepository.GetSupplierNames(p.getId());

                for (String sn : supplierNames) {
                    Supplier s = new Supplier();
                    s.setName(sn);
                    suppliers.add(s);
                }
            }
            p.setSuppliers(suppliers);
        }

        return parts;
    }

    public PartCommand getPart(Long id){
        Optional<Part> partOptional = partRepository.findById(id);
        if(!partOptional.isPresent()){
            throw new RuntimeException("Part not found");
        }
        return partToCommandConverter.convert(partOptional.get());
    }

    public PartCommand savePart(PartCommand partCommand){
        Part part = commandToPartConverter.convert(partCommand);
        Set<Supplier> suppliers = new HashSet<>();
        supplierRepository.findAllById(partCommand.getSupplierIds()).iterator().forEachRemaining(suppliers::add);
        part.setSuppliers(suppliers);
        return partToCommandConverter.convert(partRepository.save(part));
    }

    public void delete(Long id){
        partRepository.deleteById(id);
    }

}
