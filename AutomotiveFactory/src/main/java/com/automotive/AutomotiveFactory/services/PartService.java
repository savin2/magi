package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.PartCommand;
import com.automotive.AutomotiveFactory.model.Part;

import java.util.Set;

public interface PartService {
    Set<Part> getParts();
    PartCommand getPart(Long id);
    PartCommand savePart(PartCommand partCommand);
    void delete(Long id);
}
