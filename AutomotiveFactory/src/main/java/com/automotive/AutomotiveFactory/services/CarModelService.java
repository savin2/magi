package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.CarModelCommand;
import com.automotive.AutomotiveFactory.model.CarModel;

import java.util.Set;

public interface CarModelService {

    Set<CarModel> getCarModels();
    CarModel getCarModel(Long id);
    CarModel saveCarModel(CarModel car);
    CarModelCommand getCarModelCommand(Long id);
    CarModelCommand saveCarModelCommand(CarModelCommand carModelCommand);
    void deleteCarModel(Long id);
}
