package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.ClientCommand;
import com.automotive.AutomotiveFactory.model.Client;

import java.util.Set;

public interface ClientService {

    Set<Client> getClients();
    Client getClient(Long id);
    ClientCommand getClientCommand(Long id);
    Client saveClient(Client client);
    ClientCommand saveClientCommand(ClientCommand clientCommand);
    void delete(Long id);

}
