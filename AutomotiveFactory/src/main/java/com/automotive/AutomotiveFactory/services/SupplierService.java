package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.SupplierCommand;
import com.automotive.AutomotiveFactory.model.Supplier;
import com.automotive.AutomotiveFactory.repositories.SupplierFromAS;
import com.automotive.AutomotiveFactory.repositories.SupplierFromEU;

import java.util.Set;

public interface SupplierService {
    Set<Supplier> getSuppliers();
    Supplier getSupplier(Long id);
    SupplierCommand getSupplierCommand(Long id);
    Supplier save(Supplier supplier);
    void saveSupplierCommand(SupplierCommand supplierCommand);
    void delete(Long id);
}
