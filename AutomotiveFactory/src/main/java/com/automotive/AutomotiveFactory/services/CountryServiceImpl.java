package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.model.Country;
import com.automotive.AutomotiveFactory.repositories.CountryRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@AllArgsConstructor
public class CountryServiceImpl implements CountryService {
    CountryRepository countryRepository;

    @Override
    public Set<Country> getCountries() {
        Set<Country> countries = new HashSet<>();
        countryRepository.findAll().iterator().forEachRemaining(countries::add);
        return countries;
    }
}
