package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.model.Country;

import java.util.Set;

public interface CountryService {
    Set<Country> getCountries();
}
