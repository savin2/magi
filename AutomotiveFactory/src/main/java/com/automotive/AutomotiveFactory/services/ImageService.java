package com.automotive.AutomotiveFactory.services;

import org.springframework.web.multipart.MultipartFile;

public interface ImageService {
    void saveImageFile(Long carModelId, MultipartFile file);
}
