package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.ClientCommand;
import com.automotive.AutomotiveFactory.converters.ClientToCommandConverter;
import com.automotive.AutomotiveFactory.converters.CommandToClientConverter;
import com.automotive.AutomotiveFactory.model.Client;
import com.automotive.AutomotiveFactory.repositories.ClientRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

    ClientRepository clientRepository;
    ClientToCommandConverter clientToCommandConverter;
    CommandToClientConverter commandToClientConverter;
    EntityManager entityManager;

    @Override
    public Set<Client> getClients() {
        Set<Client> clients = new HashSet<>();
        clientRepository.findAll().iterator().forEachRemaining(clients::add);
        return clients;
    }

    @Override
    public Client getClient(Long l) {

        Optional<Client> clientOptional = clientRepository.findById(l);

        if (!clientOptional.isPresent()) {
            throw new RuntimeException("Client not found!");
        }

        return clientOptional.get();
    }

    @Override
    public ClientCommand getClientCommand(Long l){
        return clientToCommandConverter.convert(getClient(l));
    }


    @Override
    public Client saveClient(Client client) {
        if(client.getId() != null) {
            entityManager.createNamedStoredProcedureQuery("update_client")
                    .setParameter("new_id", client.getId())
                    .setParameter("new_name", client.getName())
                    .setParameter("new_birthdate", client.getBirthdate())
                    .setParameter("new_cnp", client.getCNP())
                    .execute();
        }
        else {
            entityManager.createNamedStoredProcedureQuery("add_client")
                    .setParameter("name", client.getName())
                    .setParameter("birthdate", client.getBirthdate())
                    .setParameter("cnp", client.getCNP())
                    .execute();
        }
        return client;
    }

    @Override
    public ClientCommand saveClientCommand(ClientCommand clientCommand){
        Client client = saveClient(commandToClientConverter.convert(clientCommand));
        return clientToCommandConverter.convert(client);
    }

    @Override
    public void delete(Long id) {
        entityManager.createNamedStoredProcedureQuery("delete_client")
                .setParameter("old_id", id)
                .execute();
    }

}
