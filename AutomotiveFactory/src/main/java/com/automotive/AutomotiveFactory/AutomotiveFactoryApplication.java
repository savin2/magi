package com.automotive.AutomotiveFactory;

import com.automotive.AutomotiveFactory.config.SecurityWebApplicationInitializer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutomotiveFactoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutomotiveFactoryApplication.class, args);
	}

}
