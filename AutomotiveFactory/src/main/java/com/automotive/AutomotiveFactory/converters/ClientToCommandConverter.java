package com.automotive.AutomotiveFactory.converters;

import com.automotive.AutomotiveFactory.commands.ClientCommand;
import com.automotive.AutomotiveFactory.model.Client;

import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class ClientToCommandConverter implements Converter<Client, ClientCommand> {

    @Nullable
    @Override
    public ClientCommand convert(Client client){

        if(client == null)
            return null;

        ClientCommand clientCommand = new ClientCommand();
        clientCommand.setId(client.getId());
        clientCommand.setName(client.getName());
        clientCommand.setBirthdate(client.getBirthdate());
        clientCommand.setCNP(client.getCNP());
        clientCommand.setPurchaseOrders(client.getPurchaseOrders());
        return clientCommand;

    }

}
