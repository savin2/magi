package com.automotive.AutomotiveFactory.converters;

import com.automotive.AutomotiveFactory.commands.CarCommand;
import com.automotive.AutomotiveFactory.model.Car;
import org.springframework.lang.Nullable;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CarToCommandConverter implements Converter<Car, CarCommand> {

    @Nullable
    @Override
    public CarCommand convert(Car car){

        if(car == null)
            return null;

        CarCommand carCommand = new CarCommand();
        carCommand.setId(car.getId());
        carCommand.setVinNumber(car.getVinNumber());
        carCommand.setFabricationDate(car.getFabricationDate());
        carCommand.setCarModel(car.getCarModel());
        carCommand.setPurchaseOrder(car.getPurchaseOrder());
        return carCommand;
    }

}
