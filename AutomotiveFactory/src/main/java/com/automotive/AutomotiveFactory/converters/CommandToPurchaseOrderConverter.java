package com.automotive.AutomotiveFactory.converters;

import com.automotive.AutomotiveFactory.commands.PurchaseOrderCommand;
import com.automotive.AutomotiveFactory.model.PurchaseOrder;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CommandToPurchaseOrderConverter implements Converter<PurchaseOrderCommand, PurchaseOrder> {

    @Nullable
    @Override
    public PurchaseOrder convert(PurchaseOrderCommand purchaseOrderCommand){
        if(purchaseOrderCommand == null)
            return null;

        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setId(purchaseOrderCommand.getId());
        purchaseOrder.setRequestDate(purchaseOrderCommand.getRequestDate());
        purchaseOrder.setCode(purchaseOrderCommand.getCode());
        purchaseOrder.setCost(purchaseOrderCommand.getCost());
        purchaseOrder.setCompleted(purchaseOrderCommand.isCompleted());
        purchaseOrder.setClient(purchaseOrderCommand.getClient());
        purchaseOrder.setFactory(purchaseOrderCommand.getFactory());
        return purchaseOrder;

    }

}
