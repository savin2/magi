package com.automotive.AutomotiveFactory.converters;

import com.automotive.AutomotiveFactory.commands.CarCommand;
import com.automotive.AutomotiveFactory.model.Car;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CommandToCarConverter implements Converter<CarCommand, Car> {

    @Nullable
    @Override
    public Car convert(CarCommand carCommand){

        if(carCommand == null)
            return null;

        Car car = new Car();
        car.setId(carCommand.getId());
        car.setFabricationDate(carCommand.getFabricationDate());
        car.setVinNumber(carCommand.getVinNumber());
        car.setPurchaseOrder(carCommand.getPurchaseOrder());
        car.setCarModel(carCommand.getCarModel());
        return car;

    }

}
