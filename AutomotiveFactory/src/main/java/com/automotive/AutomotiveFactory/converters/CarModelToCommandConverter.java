package com.automotive.AutomotiveFactory.converters;

import com.automotive.AutomotiveFactory.commands.CarModelCommand;
import com.automotive.AutomotiveFactory.model.CarModel;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class CarModelToCommandConverter implements Converter<CarModel, CarModelCommand> {

    @Nullable
    @Override
    public CarModelCommand convert(CarModel carModel){
        if(carModel == null)
            return null;

        final CarModelCommand carModelCommand = new CarModelCommand();
        carModelCommand.setId(carModel.getId());
        carModelCommand.setName(carModel.getName());
        carModelCommand.setBasePrice(carModel.getBasePrice());
        carModelCommand.setImage(carModel.getImage());
        carModelCommand.setType(carModel.getType());
        carModelCommand.setCars(carModel.getCars());
        carModelCommand.setFactories(carModel.getFactories());

        return carModelCommand;
    }

}
