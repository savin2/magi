package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.Part;

import java.util.Set;

public interface SupplierCustom {

    Long getId();
    String getName();
    String getAddress();
    String getEmail();
    Set<Part> getParts();

}
