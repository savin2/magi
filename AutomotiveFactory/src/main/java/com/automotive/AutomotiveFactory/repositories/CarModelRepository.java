package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.CarModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CarModelRepository extends CrudRepository<CarModel, Long> {
    CarModel findByName(String name);
}
