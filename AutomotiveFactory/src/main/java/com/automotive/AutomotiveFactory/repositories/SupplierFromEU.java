package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.Part;

import java.util.Set;

public interface SupplierFromEU {

    Long getId();
    String getName();
    String getAddress();
    Set<Part> getParts();

}
