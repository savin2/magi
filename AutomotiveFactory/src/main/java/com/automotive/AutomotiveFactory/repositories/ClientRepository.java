package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {
    Client findByName(String name);
}
