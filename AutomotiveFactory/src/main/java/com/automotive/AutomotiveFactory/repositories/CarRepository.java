package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.Car;
import org.springframework.data.repository.CrudRepository;

public interface CarRepository extends CrudRepository<Car, Long> {
    Car findByVinNumber(String vinNumber);
}
