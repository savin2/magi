package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.Part;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface PartRepository extends CrudRepository<Part,Long> {
    Part findByName(String name);

    Optional<Part> findById(Long id);

    Set<Part> findPartsByIdIn(List<Long> Id);

    @Query(value = "SELECT s.name from SUPPLIER s JOIN PART_SUPPLIER ps ON s.id = ps.supplier_id AND ps.part_Id = ?1", nativeQuery = true)
    List<String> GetSupplierNames(Long id);
}
