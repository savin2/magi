package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.PurchaseOrder;
import org.springframework.data.repository.CrudRepository;

public interface PurchaseOrderRepository extends CrudRepository<PurchaseOrder, Long> {
    PurchaseOrder findByCode(String code);
}
