package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.Region;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface RegionRepository extends CrudRepository<Region, Long> {

    @Query(value = "SELECT id FROM Region", nativeQuery = true)
    int getRegionId();
}