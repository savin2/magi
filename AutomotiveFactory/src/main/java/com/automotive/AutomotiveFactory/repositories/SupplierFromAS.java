package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.Part;

import java.util.Set;

public interface SupplierFromAS {

    Long getId();
    String getEmail();
    Set<Part> getParts();

}
