package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.Country;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<Country, Long> {
}