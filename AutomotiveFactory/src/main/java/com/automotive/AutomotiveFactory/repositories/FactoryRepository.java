package com.automotive.AutomotiveFactory.repositories;
import com.automotive.AutomotiveFactory.model.Factory;

import org.springframework.data.repository.CrudRepository;

public interface FactoryRepository extends CrudRepository<Factory, Long> {
    Factory findByName(String name);
}
