package com.automotive.AutomotiveFactory.repositories;

import com.automotive.AutomotiveFactory.model.Supplier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface SupplierRepository extends CrudRepository<Supplier, Long> {
    Supplier findByName(String name);

    @Query(value = "SELECT * FROM Supplier", nativeQuery = true)
    Set<SupplierFromAS> findAllASSuppliers();

    @Query(value = "SELECT * FROM Supplier", nativeQuery = true)
    Set<SupplierFromEU> findAllEUSuppliers();

    @Query(value = "SELECT s.id, seu.name, seu.address, s.email FROM Supplier s JOIN Supplier@dblink_eu seu ON s.id = seu.id WHERE s.id = ?1", nativeQuery = true)
    SupplierCustom findASSupplierById(Long id);

    @Query(value = "SELECT s.id, s.name, s.address, sas.email FROM Supplier s JOIN Supplier@dblink_as sas ON s.id = sas.id WHERE s.id = ?1", nativeQuery = true)
    SupplierCustom findEUSupplierById(Long id);

    @Query(value = "SELECT part_id FROM part_supplier where supplier_id = ?1", nativeQuery = true)
    List<Long> findPartIds(Long id);
}
