package com.automotive.AutomotiveFactory.controllers;

import com.automotive.AutomotiveFactory.model.PurchaseOrder;
import com.automotive.AutomotiveFactory.services.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    public IndexController(){}

    @RequestMapping({"", "/", "/index"})
    public String getIndexPage(){
        return "index";
    }

    @GetMapping("/showLogInForm")
    public String showLogInForm(){
        return "login";
    }

    @GetMapping("/showErrorLogIn")
    public String showErrorLogIn(Model model){
        model.addAttribute("errorMessage", "try again ... ");
        return "login";
    }

    @GetMapping("/logout")
    public String logout(){
        return "login";
    }

    @GetMapping("/accessDenied")
    public String accessDenied() {
        return "access-denied";
    }

    }
