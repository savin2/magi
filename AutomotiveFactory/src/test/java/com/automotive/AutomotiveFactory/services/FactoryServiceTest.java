package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.FactoryCommand;
import com.automotive.AutomotiveFactory.converters.CommandToFactoryConverter;
import com.automotive.AutomotiveFactory.converters.FactoryToCommandConverter;
import com.automotive.AutomotiveFactory.model.Factory;
import com.automotive.AutomotiveFactory.repositories.FactoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class FactoryServiceTest {

    public static final String NAME = "Craiova";

    @Autowired
    FactoryService factoryService;

    @Autowired
    FactoryRepository factoryRepository;

    @Autowired
    FactoryToCommandConverter factoryToCommandConverter;
    @Autowired
    CommandToFactoryConverter commandToFactoryConverter;

    @Test
    public void testSaveFactory(){
        log.info("Factory save test");
        Factory factory = factoryRepository.findByName("Mioveni");
        FactoryCommand factoryCommand = factoryToCommandConverter.convert(factory);
        factoryCommand.setName(NAME);
        FactoryCommand savedFactoryCommand = factoryService.saveFactoryCommand(factoryCommand);
        assertEquals(NAME, savedFactoryCommand.getName());
        log.info("Factory save test");
    }

}
