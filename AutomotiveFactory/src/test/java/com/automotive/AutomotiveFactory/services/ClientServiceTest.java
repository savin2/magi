package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.ClientCommand;
import com.automotive.AutomotiveFactory.converters.ClientToCommandConverter;
import com.automotive.AutomotiveFactory.converters.CommandToClientConverter;
import com.automotive.AutomotiveFactory.model.Client;
import com.automotive.AutomotiveFactory.repositories.ClientRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class ClientServiceTest {

    public static final String name = "Client";

    @Autowired
    ClientService clientService;

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ClientToCommandConverter clientToCommandConverter;
    @Autowired
    CommandToClientConverter commandToClientConverter;

    @Test
    public void testSaveFactory(){
        log.info("Client save test");
        Client client = clientRepository.findByName("Iatsa");
        ClientCommand clientCommand = clientToCommandConverter.convert(client);
        clientCommand.setName(name);
        ClientCommand savedClientCommand = clientService.saveClientCommand(clientCommand);
        assertEquals(name, savedClientCommand.getName());
        log.info("Factory save test");
    }

}
