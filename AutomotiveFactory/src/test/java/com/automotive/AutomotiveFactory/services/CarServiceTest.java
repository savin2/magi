package com.automotive.AutomotiveFactory.services;

import com.automotive.AutomotiveFactory.commands.CarCommand;
import com.automotive.AutomotiveFactory.converters.CarToCommandConverter;
import com.automotive.AutomotiveFactory.converters.CommandToCarConverter;
import com.automotive.AutomotiveFactory.converters.CommandToCarModelConverter;
import com.automotive.AutomotiveFactory.model.Car;
import com.automotive.AutomotiveFactory.repositories.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class CarServiceTest {

    public static final String vinNumber = "1111";

    @Autowired
    CarService carService;

    @Autowired
    CarRepository carRepository;

    @Autowired
    CarToCommandConverter carToCommandConverter;
    @Autowired
    CommandToCarConverter commandToCarConverter;

    @Test
    public void testSaveFactory(){
        log.info("Car save test");
        Car car = carRepository.findByVinNumber("0000");
        CarCommand carCommand = carToCommandConverter.convert(car);
        carCommand.setVinNumber(vinNumber);
        CarCommand savedCarCommand = carService.saveCarCommand(carCommand);
        assertEquals(vinNumber, savedCarCommand.getVinNumber());
        log.info("Factory save test");
    }

}
